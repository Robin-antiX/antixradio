# antiXradio
# Copyright (C) 2023 antiX Community
# This file is distributed under the same license as the antixradio package (GPL v.3)
# Wallon, 2023.
# 
# Translators:
# anticapitalista <anticapitalista@riseup.net>, 2023
# Geoff Gigg <geoffgigg@hotmail.com>, 2023
# Henry Oquist <henryoquist@nomalm.se>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: version 0.45\n"
"Report-Msgid-Bugs-To: https://www.antixforum.com\n"
"POT-Creation-Date: 2023-12-30 08:42+0100\n"
"PO-Revision-Date: 2023-12-24 11:50+0000\n"
"Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023\n"
"Language-Team: Swedish (https://app.transifex.com/anticapitalista/teams/10162/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Text displayed in a button in main window.
#: antiXradio:55
msgid "Exit"
msgstr "Avsluta"

#. Text displayed in a button in main window.
#: antiXradio:56
msgid "Play"
msgstr "Spela"

#. Text displayed in a button in main window.
#: antiXradio:57
msgid "Stop"
msgstr "Stopp"

#. Text displayed in a button in main window.
#: antiXradio:58
msgid "Load another list"
msgstr "Ladda en annan lista"

#. Text displayed in a button in main dialog and info dialog both.
#: antiXradio:59
msgid "Turn off the radio"
msgstr "Stäng av radion"

#. Text displayed in a button in info dialog that appears after clicking the X
#. in upper Window border of main window
#: antiXradio:60
msgid "Return"
msgstr "Återvänder"

#. Header line of stations list displayed in main window.
#: antiXradio:61
msgid "Radio stations:"
msgstr "Radiostationer"

#. Window title supplement in main window's upper border, present if reception
#. was stopped.
#: antiXradio:62
msgid "(Standby)"
msgstr "(Standby)"

#. Text displayed in a pulldown in file selection dialog for all files with
#. the ".txt" extension (no other choices)
#: antiXradio:63
msgid "Text files"
msgstr "Textfiler"

#. Text of file selection dialog for selecting another stations list.
#: antiXradio:64
msgid "Select the radio list you want to load:"
msgstr "Välj vilken radiolista du vill ladda."

#. Text displayed in a button in main dialog and info dialog both.
#: antiXradio:65
msgid "Close window"
msgstr "Stäng fönster"

#. Window title supplement in main window's upper border, present if reception
#. was stopped.
#: antiXradio:66
msgid "stopped"
msgstr "stoppad"

#. Text displayed in a button in main window.
#: antiXradio:67
msgid "Record"
msgstr "Spela in"

#. Tooltip for main window's exit button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:68
msgid ""
"This will power off the radio and stop any running station or record. If you"
" want just to close this window instead, keeping the radio playing, use the "
"X in upper window border or the ESC key on your keyboard instead. To stop "
"reception later call antiXradio again, and press this button then."
msgstr ""
"Detta kommer att stänga av radion och stoppa alla stationer eller "
"inspelningar som körs. Om du istället bara vill stänga detta fönster och "
"låta radion fortsätta spela, använd X:et i övre fönsterkant eller ESC-"
"tangenten på ditt tangentbord istället.. För att senare stoppa mottagningen "
"anropa antiXradio igen, och tryck då på denna knapp.."

#. Tooltip for main window's change station button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:71
msgid ""
"This will allow you to select another stations list from a folder on your "
"file system. You may edit the stations lists within a text editor, just make"
" sure to keep the format you find in it."
msgstr ""
"Detta kommer att låta dig välja en annan stationslista från en mapp i ditt "
"filsystem. Du kan redigera stationslistan med en textredigerare, bara du "
"behåller ursprungsformatet."

#. Tooltip for main window's stop button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:73
msgid ""
"Stop current radio reception or recording. Starting another station or "
"another recording will also stop the current reception."
msgstr ""
"Stoppa nuvarande radiomottagning eller inspelning. Att ta in en annan "
"station eller starta en annan inspelning kommer också att stoppa nuvarande "
"mottagning."

#. Tooltip for main window's recording button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:75
msgid ""
"Record what the selected station plays to a file in your home folder's "
"“Radio-Recordings” directory. The file will have .ts extension and can be "
"played with MPV media player. You may convert it after recording was "
"completed to whatever format (mp3, mka, ogg m4a) by means of a audio "
"conversion tool (e.g. ffmpeg or audacity)"
msgstr ""
"Spela in det vald station spelar till en fil  i din hemkatalogs "
"\"Radioinspelningar\"-mapp. Filen kommer att ha ändelsen .ts och kan spelas "
"med MPV mediaspelare. Du kan konvertera den efter inspelningen avslutats "
"till valfritt format (mp3, mka, ogg m4a) geno en ljudomvandlingsverktyg (dvs"
" ffmpeg eller audacity)"

#. Tooltip for main window's start reception button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:79
msgid ""
"This will start reception of selected radio station. You need to be "
"connected to the internet to receive the radio streams via network."
msgstr ""
"Detta kommer att starta mottagning av vald radiostation. Du måste vara "
"ansluten till internet för att strömma radio via nätverk."

#. Button-tooltip in stations list update warning dialog at startup, right
#. button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#. Please don't translate the path ~/.config/antiXradio/ and also not the file
#. extension .bak.
#: antiXradio:81
msgid ""
"This will bluntly overwrite all stations lists in "
"~/.config/antiXradio/stations (all the old files will be saved to .bak "
"extension. Existing .bak files will get overwriten)"
msgstr ""
"Detta kommer att skriva över alla stationslistor i "
"~/.config/antiXradio/stations (alla gamla filer kommer att sparas med "
"ändelsen .bak. Existerande .bak filer blir överskrivna)"

#. Button-tooltip in stations list update warning dialog at startup, middle
#. button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:83
msgid ""
"Stations lists you have modified will not be replaced, instead the update "
"will be written next to them with the extension .new so you can compare "
"using e.g. »meld«, and transfer from the update to your individual lists "
"what you feel like."
msgstr ""
"Stationslistor du har ändrat kommer inte att bytas ut, istället kommer "
"uppdateringen skrivas intill dem med ändelsen .new så du kan jämföra genom "
"att t.ex. använda. »meld«, och överföra det du vill till dina individuella "
"listor."

#. Button-tooltip in stations list update warning dialog at startup, left
#. button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#. Please don't translate the paths ~/.config/antiXradio/stations and
#. /usr/local/lib/antiXradio/stations
#: antiXradio:86
msgid ""
"Please care yourself for updating your collection of lists residing in "
"~/.config/antiXradio/stations from the new files present in "
"/usr/local/lib/antiXradio/stations"
msgstr ""
"Var vänlig se till att uppdatera din samling av listor som finns i "
"~/.config/antiXradio/stations med de nya filer som finns i "
"/usr/local/lib/antiXradio/stations"

#. Tooltip for left button in info dialog that appears after clicking the X in
#. upper Window border of main window
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:88
msgid "Stop radio broadcasts and recordings if any, exit antiXradio"
msgstr ""
"Stoppa radiosändningar och inspelningar om det finns några, stäng av "
"antiXradio"

#. Tooltip for middle button in info dialog that appears after clicking the X
#. in upper Window border of main window
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:89
msgid "Cancel action and return to the main window"
msgstr "Stoppa aktiviteten och återvänd till huvudfönstret"

#. Tooltip for right butto in info dialog that appears after clicking the X in
#. upper Window border of main window
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:90
msgid ""
"Listen to the radio station without distracting window. Launch the radio "
"later again from antiX main menu to change radio channel, stop radio "
"reception or recording, or to exit antiXradio."
msgstr ""
"Lyssna på radiostationen utan störande fönster. Starta radion igen senare "
"från antiX huvudmeny för att byta kanal, stoppa radiomottagnng eller "
"inspelning, eller för att avsluta antiXradio."

#. Base folder name for storing recordings (sub-folder of user's home folder)
#: antiXradio:108
msgid "Radio-Recordings"
msgstr "Radio-Inspelningar"

#. One or more software packages were not found. The name of the package
#. missing preceeds this.
#. Command line error message text
#: antiXradio:129
msgid "not found."
msgstr "kunde inte hittas."

#. Do not translate the "\n" code. These are line breaks. If your text is too
#. long, you can add "\n" codes.
#. Command line error text
#: antiXradio:131
msgid ""
"The installation of this script has failed. Make sure the above "
"command(s)\\nis (are) available on your system and please contact package "
"maintainer.\\nantiXradio Leaving."
msgstr ""
"Installationen av detta skript har misslyckats. Kontrollera att ovanstående "
"kommando(n)\\när (är) tillgängliga på ditt system och var vänlig kontakta "
"paketansvarige.\\nantiXradio Avslutar.."

#. translatable window header, used in main and exit info window both. I gues
#. only the part "radio" can get translated, since antiX is a name.
#: antiXradio:199 antiXradio:216 antiXradio:224 antiXradio:297 antiXradio:369
#: antiXradio:377 antiXradio:467 antiXradio:468 antiXradio:469 antiXradio:470
#: antiXradio:617 antiXradio:635
msgid "antiXradio"
msgstr "antiXradio"

#. Text in info dialog that appears after clicking the X in upper Window
#. border of main window
#. Do not translate the codes "\n\t" or "\n". These are line breaks. If your
#. text is too long, you can add codes "\n\t" or "\n".
#. Never translate "$exit_button_text". Also <b> and </b> is not translatable,
#. these are tags for bold text in between. The order
#. of opening <b> and closing tags </b> MUST be observed properly.
#: antiXradio:200
#, sh-format
msgid ""
"\\n\\t<b> Please note! </b>\\n\\n\\t When using the ESC key or the X from "
"upper window\\n\\t borders, only the window will be closed while the\\n\\t "
"radio keeps playing (and/or recording).\\n\\t To power off the radio later, "
"please start antiX Radio \\n\\t and click the '<b>$exit_button_text</b>' "
"button on the lower \\n\\t left section of it's window. \\n\\n"
msgstr ""
"\\n\\t<b> Var vänlig notera! </b>\\n\\n\\t När ESC-tangenten används eller "
"X:et i övre fönster\\n\\t kant, kommer enbart fönstret att stängas "
"medan\\n\\t radion fortsätter att spela (och/eller spela in).\\n\\t För att "
"stänga av radion senare, var vänlig starta antiX Radio \\n\\t och klicka på "
"'<b>$exit_button_text</b>' knappen i den nedre \\n\\t vänstra delen av dess "
"fönster. \\n\\n"

#. Checkbox text in info dialog that appears after clicking the X in upper
#. Window border of main window
#: antiXradio:201
msgid "Got it. Don’t show me again."
msgstr "Har förstått. Visa det inte igen."

#. Headertext in info dialog (visible when not connection to internet was
#. found on startup)
#: antiXradio:242
msgid "Internet connection error."
msgstr "Internetanslutningsfel"

#. Text in info dialog (visible when not connection to internet was found on
#. startup)
#: antiXradio:243
msgid "No Internet connection found."
msgstr "Ingen Internetansluttning hittad."

#. Do not translate the "\n" code. These are line breaks. If your text is
#. too long, you can add "\n" codes.
#. Text in info dialog (visible when not connection to internet was found on
#. startup)
#: antiXradio:243
msgid ""
"Please connect to the Internet before\\ntrying again or you may exit "
"antiXradio."
msgstr ""
"Var vänlig anslut till Internet innan\\ndu försöker igen eller så kan du "
"avsluta antiXradio."

#. Text displayed in a button in internet connection error dialog.
#: antiXradio:244
msgid "Exit antiXradio"
msgstr "Avsluta antiXradio"

#. Text displayed in a button in internet connection error dialog.
#: antiXradio:244
msgid "Try again"
msgstr "Försök igen"

#. Base Filename for recording, followed by a date in locale format. Please
#. make sure never to use a slash "/" in your translation of this string.
#: antiXradio:333
msgid "radio-recording"
msgstr "radio-inspelning"

#. Title of yad notifiction in system's status bar.
#: antiXradio:351
msgid "antiXradio recording"
msgstr "antiXradio inspelning"

#. Tooltip of notification icon in system's status bar, to be translated to
#. the meaning "There is a recording running".
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:352 antiXradio:357
msgid "Running antiXradio recording"
msgstr "Köra antiXradio inspelning"

#. Stations list update warning dialog at startup, window border text.
#: antiXradio:483
msgid "antiXradio stations list update"
msgstr "uppdatera antiXradios stationslista"

#. Stations list update warning dialog at startup, header text in bold.
#: antiXradio:484
msgid "antiXradio's stations lists have been actualised."
msgstr "antiXradios stationslistor har uppdaterats."

#. Stations list update warning dialog at startup, dialog text.
#. Please make sure to place all the \n line breaks in proper position for
#. your
#. language to make your text fit in the window neatly. You can use more of \n
#. line breaks if needed, also line length is to a certain degree variable.
#. Don't translate \\t/usr/local/lib/antiXradio/stations\n and
#. \\t~/.config/antiXradio/stations\n paths.
#: antiXradio:485
msgid ""
"The new lists are waiting in\n"
"\\t/usr/local/lib/antiXradio/stations\n"
"directory to be copied to your personal collection\n"
"of stations lists in your home folder\n"
"\\t~/.config/antiXradio/stations\n"
"From the buttons’ tooltips you can learn the options\n"
"you have. Do you want antiXradio to do the update of\n"
"your collection for you?"
msgstr ""
"De nya listorna finns i\n"
"\\t/usr/local/lib/antiXradio/stations\n"
"katalogen för att kopieras till din egen samling\n"
"ostationslistor i din hemkatalog\n"
"\\t~/.config/antiXradio/stations\n"
"Du kan lära dig alternativen från knappens verktygstips\n"
"Vill du att antiXradio ska uppdatera\n"
"din samling åt dig?"

#. Button engraving in stations list update warning dialog at startup
#: antiXradio:493
msgid "No thanks"
msgstr "Nej tack"

#. Button engraving in stations list update warning dialog at startup
#: antiXradio:494
msgid "Do the math for me"
msgstr "Räkna ut det för mig"

#. Button engraving in stations list update warning dialog at startup
#: antiXradio:495
msgid "Just overwrite"
msgstr "Skriv bara över"

#. Checkbox label in main window
#: antiXradio:632
msgid ""
"Displays a small window with information on the current radio programme and "
"the station."
msgstr ""
"Visar ett smalt fönster med information om nuvarande radioprogram aoch "
"station."

#. Window title supplement in main window's upper border, present if a
#. recording is running. Please keep the double exclamation marks for better
#. perceptibility (or use another eyecatcher of your choice)
#: antiXradio:668
msgid "!! Recording !!"
msgstr "!! Inspelning !!"
